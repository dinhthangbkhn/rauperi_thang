import { Component } from '@angular/core';
import {LoginComponent} from './components/login/login.component';
// import './rxjs-operators';

@Component({
    selector: 'my-app',
    templateUrl: 'app.component.html'
})
export class AppComponent {
    name: string = "Angular 2 on Express";

    constructor() {}
}
