/**
 * Created by thangdt on 14/01/2017.
 */
/**
 * Created by thangdt on 12/01/2017.
 */
import { Component } from '@angular/core';
import 'rxjs/add/operator/map';
import {CookieService} from "angular2-cookie/core";

import {Userlogin} from "../../model/Userlogin";
import {Router} from "@angular/router";

import {StoreService} from "../../service/store.service";
import {MyproductService } from "../../service/myproduct.service";
import {UserService} from "../../service/user.service";
import {BillService} from "../../service/bill.service";

import {Product} from "../../model/Product";
import {User} from "../../model/User";
import {Bill} from "../../model/Bill";
import {NewBillResponse} from "../../model/NewBillResponse";
import {ListBillResponse} from "../../model/ListBillResponse";

@Component({
  selector: 'store',
  templateUrl: 'components/store/store.component.html',
  styleUrls: ['components/store/store.component.css'],
  providers: [StoreService, BillService, MyproductService, UserService]
})
export class StoreComponent {
  public listProduct:Product[];
  public currentProduct:Product = new Product("","","",0,0,"");
  public currentUser:User;
  public number:number = 0;
  public newBill:Bill;
  public time:string;
  public date:Date;
  public objUser:User;
  public newBillResponse: NewBillResponse;
  public listBillResponse: ListBillResponse;


  constructor(
    public _myproductService: MyproductService,
    public _userService: UserService,
    public _billService: BillService,
    public _cookieService: CookieService
  ) {
    this.date = new Date();
    this.time = this.date.toLocaleString();
    this. _userService.getInfor(this._cookieService.get("userlogin")).subscribe(
      (data) => this.objUser=data,
      (err) => console.log("error"),
      ()=> this.getAllProducts());
  }

  public createBill() {
    this.newBill = new Bill(this.objUser._id,this.currentProduct._id,this.number,this.time);
    this._billService.createBill(this.newBill).subscribe(
      (data) => this.newBillResponse = data,
      (err) => console.log(err),
      () => console.log("created bill")
    )
  }

  public getAllProducts() {
    this._myproductService.getAllProduct().subscribe(
      (data) => {this.listProduct = data.listProduct;console.log(data)},
      (err) => console.log(err),
      () => {
        for(let product of this.listProduct) {
          this._userService.getInforById(product.userid).subscribe(
            (data) => product.username = data.username
          )
        }
      }
    )
  }

  public productDetails(product:Product) {
    this.currentProduct = product;
  }

}
