/**
 * Created by thangdt on 17/01/2017.
 */


import { Component } from '@angular/core';
import 'rxjs/add/operator/map';
import {User} from "../../model/User";
import {StoreService} from "../../service/store.service";
import {CookieService} from "angular2-cookie/core";
import {UserService} from "../../service/user.service";
import { CommentService } from "../../service/comment.service";
import { Post } from "../../model/Post";
import { PostService } from "../../service/post.service";

import { NewPostResponse } from "../../model/NewPostResponse";
import { ListPostResponse } from "../../model/ListPostResponse";
import { DeletePostResponse } from "../../model/DeletePostResponse";
import { Comment } from "../../model/Comment";
import { NewCommentResponse } from "../../model/NewCommentResponse";
import { ListCommentResponse } from "../../model/ListCommentResponse";

@Component({
  selector: 'post',
  templateUrl: 'components/post/post.component.html',
  styleUrls: ['components/post/post.component.css'],
  providers: [StoreService, UserService, CommentService]
})
export class PostComponent {
  public title:string;
  public content:string;
  public time: string;
  public date = new Date();
  public newPost:Post;
  public userObj: User;
  public listPost: Post[];
  public newPostResponse: NewPostResponse;
  public listPostResponse: ListPostResponse;
  public deletePostResponse: DeletePostResponse;

  public newComment:Comment;
  public newCommentResponse: NewCommentResponse;
  public listCommentResponse: ListCommentResponse;
  public comment:string;

  constructor(private _cookieService:CookieService, private _userservice:UserService, private _postService:PostService,
  private _commentService:CommentService) {
    this.time = this.date.toLocaleString();
    _userservice.getInfor(this._cookieService.get("userlogin")).subscribe(
      (data) => this.userObj=data,
      (err) => console.log("error"),
      ()=> this.getListPost());
  }

  createPost() {
    this.newPost = new Post(this.title, this.content, this.time, this.userObj._id);
    // console.log(this.newPost);
    this._postService.createPost(this.newPost).subscribe(
      (data) => this.newPostResponse = data,
      (err) => console.log(err),
      () => this.listPost.push(this.newPost)
    );
  }

  createComment(post:Post) {
    this.newComment = new Comment(this.comment, post._id, this.userObj._id);
    this._commentService.createComment(this.newComment).subscribe(
      (data) => this.newCommentResponse = data,
      (err) => console.log(err),
      () => post.listComment.push(this.newComment)
    )
  }

  getListPost() {
    this._postService.getListPost().subscribe(
      (data) => this.listPostResponse=data,
      (err) => console.log(err),
      () => {
        this.listPost = this.listPostResponse.listPost;
        this.getListUsernameForListPost();
        for (let post of this.listPost) {
          this.getListComment(post);
        }
      }
    )
  }

  getListComment(post:Post) {
    this._commentService.getListComment(post).subscribe(
      (data)=>this.listCommentResponse = data,
      (err) => console.log(err),
      () => {
        this.getListUsernameForListComment();
        post.listComment = this.listCommentResponse.listComment;
      }

    )
  }

  deletePost(deletePost: Post):void {
    this._postService.deletePost(deletePost._id).subscribe(
      (data) => this.deletePostResponse = data,
      (err) => console.log(err),
      () => {
        var index = this.listPost.indexOf(deletePost,0);
        this.listPost.splice(index,1);
      }
    )
  }

  getListUsernameForListPost() {
    for(let post of this.listPost) {
      this._userservice.getInforById(post.userid).subscribe(
          (data) => post.username = data.username,
          (err) => console.log(err),
          () => console.log()
        )
    }
  }

  getListUsernameForListComment() {
    for(let comment of this.listCommentResponse.listComment) {
      this._userservice.getInforById(comment.userid).subscribe(
        (data) => comment.username = data.username,
        (err) => console.log(err),
        () => console.log()
      )
    }
  }

}
