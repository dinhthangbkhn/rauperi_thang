/**
 * Created by thangdt on 20/01/2017.
 */
import { Component } from '@angular/core';
import 'rxjs/add/operator/map';
import {CookieService} from "angular2-cookie/core";

import {StoreService} from "../../service/store.service";
import {MyproductService } from "../../service/myproduct.service";
import {UserService} from "../../service/user.service";
import {BillService} from "../../service/bill.service";

import {Product} from "../../model/Product";
import {User} from "../../model/User";
import {Bill} from "../../model/Bill";
import {NewBillResponse} from "../../model/NewBillResponse";
import {ListBillResponse} from "../../model/ListBillResponse";

@Component({
  selector: 'bought',
  templateUrl: 'components/bought/bought.component.html',
  styleUrls: ['components/bought/bought.component.css'],
  providers: [StoreService, BillService, MyproductService, UserService]
})
export class BoughtComponent {
  listProduct: Product[];
  currentProducts: Product;
  listBills: Bill[];
  currentBill: Bill;
  userObj:User;
  constructor(
    private _userService: UserService,
    private _billService: BillService,
    private _productService: MyproductService,
    private _cookieService: CookieService
  ) {
    this. _userService.getInfor(this._cookieService.get("userlogin")).subscribe(
      (data) => this.userObj=data,
      (err) => console.log("error"),
      ()=> this.getAllBillByUserid());
  }

  getAllBillByUserid() {
    this._billService.getListBillByUserid(this.userObj).subscribe(
      (data) => this.listBills = data.listBills,
      (err) => console.log(err),
      () => {
        console.log("hello");
        for( let bill of this.listBills) {
          this._productService.getProductInforById(bill.productid).subscribe(
            (data) => {bill.productname = data.name;bill.productprice=data.price},
            (err) => console.log(err),
            () => console.log(bill.productname)
          )
        }
      }
    )
  }
}
