/**
 * Created by thangdt on 14/01/2017.
 */

import { Component } from '@angular/core';
import 'rxjs/add/operator/map';
import {User} from "../../model/User";
import {StoreService} from "../../service/store.service";
import {Product} from "../../model/Product";
import {CookieService} from "angular2-cookie/core";
import {UserService} from "../../service/user.service";
import {MyproductService} from "../../service/myproduct.service";
import {NewProductResponse} from "../../model/NewProductResponse";
import {ListProductResponse } from "../../model/ListProductResponse";

@Component({
  selector: 'myproduct',
  templateUrl: 'components/myproduct/myproduct.component.html',
  styleUrls: ['components/myproduct/myproduct.component.css'],
  providers: [StoreService, UserService]
})
export class MyproductComponent {
  name:string = "";
  imgUrl:string = "/img/pikachu.png";
  describe:string = "";
  number:number = 0;
  price:number = 0;

  userObj: User;
  newProduct: Product;
  listProduct: Product[];
  newProductResponse: NewProductResponse;
  listProductResponse: ListProductResponse;

  constructor(private _cookieService:CookieService, private _userservice:UserService, private _myproductService:MyproductService) {
    _userservice.getInfor(this._cookieService.get("userlogin")).subscribe(
      (data) => this.userObj=data,
      (err) => console.log("error"),
      ()=> this.getListProduct());
  }

  createProduct() {
    this.newProduct = new Product(this.name,this.imgUrl,this.describe,this.number,this.price,this.userObj._id);
    console.log(this.newProduct);
    this._myproductService.createProduct(this.newProduct).subscribe(
      (data) => this.newProductResponse=data,
      (err) => console.log(err),
      () => {
        this.listProduct.push(this.newProduct);
        this.newProduct.username = this.userObj.username;
      }
    );
  }

  getListProduct() {
    this._myproductService.getListProduct(this.userObj._id).subscribe(
      (data) => this.listProductResponse=data,
      (err) => console.log(err),
      () => {
        this.listProduct = this.listProductResponse.listProduct;
        for (let product of this.listProduct) {
          product.username = this.userObj.username;
        }
      }
      // () => console.log(this.listProductResponse.listProduct[0])
    );
  }

}
