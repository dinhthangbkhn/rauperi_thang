/**
 * Created by thangdt on 12/01/2017.
 */
import { Component } from '@angular/core';
import 'rxjs/add/operator/map';
import { LoginService } from '../../service/login.service';
import {Userlogin} from "../../model/Userlogin";
import {Router} from "@angular/router";
import {CookieService} from "angular2-cookie/core";
import {User} from "../../model/User";

@Component({
  selector: 'login',
  templateUrl: 'components/login/login.component.html',
  styleUrls: ['components/login/login.component.css'],
  providers: [LoginService]
})
export class LoginComponent {

  username: string = "Thang";
  password: string = "123456";
  data = {
    isCorrect: false,
    error: ""
  };
  user = new Userlogin(this.username, this.password);
  constructor(private loginService: LoginService, private router:Router, private _cookieService:CookieService) {
  }
  onSubmit() {
    // console.log(this.http.get("/users").map(data => data.json()).subscribe((data) => this.users = data));
    //   console.log(this.user);
    // console.log(this.loginService.getInfor().subscribe((data) => console.log(data)));
    this.loginService.sendInfor(this.user).subscribe((data)=> this.data=data);
    console.log(this.data);
    if(this.data.isCorrect == true) {
      this._cookieService.put("userlogin",this.user.username);
      console.log(this._cookieService.get("userlogin"));
      this.router.navigate(['myproduct']);
      // console.log("check coommit");
    }
  }


}
