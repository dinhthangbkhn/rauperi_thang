/**
 * Created by thangdt on 20/01/2017.
 */
import { Component } from '@angular/core';
import 'rxjs/add/operator/map';
import { LoginService } from '../../service/login.service';
import {Userlogin} from "../../model/Userlogin";
import {Router} from "@angular/router";
import {CookieService} from "angular2-cookie/core";
import {User} from "../../model/User";
import {UserSubmit} from "../../model/UserSubmit";

@Component({
  selector: 'submit',
  templateUrl: 'components/submit/submit.component.html',
  styleUrls: ['components/submit/submit.component.css'],
  providers: [LoginService]
})
export class SubmitComponent {

  username: string = "Thang";
  password: string = "123456";
  fullname: string = "Dinh Trong Thang";
  phone: string = "0123456789";
  address: string = "Hanoi";
  newUser:UserSubmit;
  isSucceed:boolean;

  constructor(private loginService: LoginService, private router:Router, private _cookieService:CookieService) {
  }
  createUser() {
    this.newUser = new UserSubmit(this.username,this.password,this.fullname,this.phone,this.address);
    this.loginService.createUser(this.newUser).subscribe(
      (data) => console.log(data),
      (err) => console.log(err)
    );

  // onSubmit() {
  //   // console.log(this.http.get("/users").map(data => data.json()).subscribe((data) => this.users = data));
  //   //   console.log(this.user);
  //   // console.log(this.loginService.getInfor().subscribe((data) => console.log(data)));
  //   this.loginService.sendInfor(this.user).subscribe((data)=> this.data=data);
  //   console.log(this.data);
  //   if(this.data.isCorrect == true) {
  //     this._cookieService.put("userlogin",this.user.username);
  //
  //     this.router.navigate(['myproduct']);
  //     // console.log("check coommit");
  //   }
  }


}
