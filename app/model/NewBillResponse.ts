/**
 * Created by thangdt on 19/01/2017.
 */
import {Bill} from "./Bill";
export class NewBillResponse {
  constructor(
    public isSucceed:boolean,
    public listBills:Bill
  ) {

  }
}
