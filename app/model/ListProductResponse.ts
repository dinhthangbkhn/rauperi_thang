/**
 * Created by thangdt on 17/01/2017.
 */
import {Product} from './Product';
export class ListProductResponse {
  constructor(
    public isSucceed:boolean,
    public listProduct:Product[]
  ) {

  }
}
