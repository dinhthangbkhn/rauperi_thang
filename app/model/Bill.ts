/**
 * Created by thangdt on 19/01/2017.
 */
import { Product } from "./Product";
export class Bill {
  public _id:string;
  public username:string;     //ten nguoi mua hang
  public productname:string;  //ten mat hang da mua
  public comment:string[];
  public productprice:number;
  constructor(
    public userid:string,     //id nguoi mua hang
    public productid:string,
    public number:number,
    public time:string
  ) {

  }
}
