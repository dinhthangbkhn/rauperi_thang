/**
 * Created by thangdt on 17/01/2017.
 */
import { Comment } from './Comment';
export class Post {
  public _id:string;
  public username: string;
  public listComment: Comment[];
  constructor(
    public title:string,
    public content:string,
    public time: string,
    public userid:string
  ) {

  }
}
