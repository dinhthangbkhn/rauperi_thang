/**
 * Created by thangdt on 18/01/2017.
 */
import {Post} from '../model/Post';
export class ListPostResponse {
  constructor(
    public isSucceed:boolean,
    public listPost:Post[]
  ) {

  }
}
