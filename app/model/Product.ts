/**
 * Created by thangdt on 14/01/2017.
 */
export class Product {
  // public _id:string;
  public username:string;
  public _id:string;
  constructor(
    public name:string,
    public imgUrl:string,
    public describe:string,
    public number:number,
    public price:number,
    public userid:string
  ) {

  }
}
