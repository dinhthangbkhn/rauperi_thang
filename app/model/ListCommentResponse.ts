/**
 * Created by thangdt on 18/01/2017.
 */
import { Comment } from "./Comment";
export class ListCommentResponse {
  constructor(
    public isSucceed:boolean,
    public listComment:Comment[]
  ) {

  }
}
