/**
 * Created by thangdt on 14/01/2017.
 */
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { Userlogin } from '../model/Userlogin';
import { Product } from '../model/Product';
import { NewProductResponse } from '../model/NewProductResponse';
import { ListProductResponse } from '../model/ListProductResponse';

@Injectable()
export class MyproductService {
  private createProductUrl:string = '/products/create';
  private listProduct:string = '/products/listProduct/';
  private listAllProductUrl:string = '/products/getAllProducts';
  private getProductInforByIdUrl:string = '/products/id/';
  constructor(private http: Http){};

  createProduct(newProduct: Product):Observable<NewProductResponse> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    console.log(newProduct);
    return this.http.post(this.createProductUrl, { newProduct }, options)
      .map(this.extractData);
  }

  getListProduct(userid: string):Observable<ListProductResponse> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.get(this.listProduct+userid).map(this.extractData);
  }

  getAllProduct():Observable<ListProductResponse> {
    return this.http.get(this.listAllProductUrl).map(this.extractData);
  }

  getProductInforById(productid:string):Observable<Product> {
    return this.http.get(this.getProductInforByIdUrl + productid).map(this.extractData);
  }

  private extractData(res: Response) {
    let body = res.json();
    // console.log(body);
    return body || { };
  }

  private handleError (error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
