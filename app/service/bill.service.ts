/**
 * Created by thangdt on 19/01/2017.
 */

import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';


import { Bill } from '../model/Bill';
import { ListBillResponse} from '../model/ListBillResponse';
import { NewBillResponse } from '../model/NewBillResponse';

import { Product } from '../model/Product';
import {User} from '../model/User';
import {UserService} from '../service/user.service';
import {MyproductService} from '../service/myproduct.service';

@Injectable()
export class BillService {

  createBillUrl:string = 'bills/create';
  getListBillByProductidUrl:string = 'bills/getlistbillbyproductid/';
  getListBillByUseridUrl:string = 'bills/getlistbillbyuserid/';
  constructor(private http: Http){};

  public createBill(newBill:Bill):Observable<NewBillResponse> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.createBillUrl,{newBill},options)
      .map(this.extractData);
  }

  public getListBillByProductid(product :Product):Observable<ListBillResponse> {
    return this.http.get(this.getListBillByProductidUrl + product._id)
      .map(this.extractData);
  }

  public getListBillByUserid(user:User):Observable<ListBillResponse> {
    return this.http.get(this.getListBillByUseridUrl+user._id)
      .map(this.extractData);
  }

  private extractData(res: Response) {
    let body = res.json();
    // console.log(body);
    return body || { };
  };

  private handleError (error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  };
}
