/**
 * Created by thangdt on 18/01/2017.
 */
/**
 * Created by thangdt on 17/01/2017.
 */
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';


import { Post } from '../model/Post';
import { Comment } from '../model/Comment';
import { NewCommentResponse } from '../model/NewCommentResponse';
import { ListCommentResponse } from '../model/ListCommentResponse';

@Injectable()
export class CommentService {

  createCommentUrl:string = 'comments/create';
  getListCommentUrl:string = 'comments/getListComment/';
  constructor(private http: Http){};

  public createComment(newComment:Comment):Observable<NewCommentResponse> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.createCommentUrl,{newComment},options)
      .map(this.extractData);
  };

  public getListComment(post:Post): Observable<ListCommentResponse> {
    return this.http.get(this.getListCommentUrl+post._id).map(this.extractData);
  }

  private extractData(res: Response) {
    let body = res.json();
    // console.log(body);
    return body || { };
  };

  private handleError (error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  };
}
