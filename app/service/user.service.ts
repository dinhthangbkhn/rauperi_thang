/**
 * Created by thangdt on 14/01/2017.
 */
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { Userlogin } from '../model/Userlogin';
import { LoginResponse} from '../model/LoginResponse';
import { User } from '../model/User';

@Injectable()
export class UserService {
  private getUserInfoUrl:string = "/users/";
  private getUserInfoByIdUrl:string = "/users/id/";
  constructor (private  http: Http){}

  getInfor(username:string):Observable<User> {
    return this.http.get(this.getUserInfoUrl+username)
      .map(this.extractData);
  }

  getInforById(userid:string):Observable<User> {
    return this.http.get(this.getUserInfoByIdUrl+userid)
      .map(this.extractData);
  }

  // sendInfor(user: Userlogin): Observable<LoginResponse> {
  //   let headers = new Headers({ 'Content-Type': 'application/json' });
  //   let options = new RequestOptions({ headers: headers });
  //
  //   return this.http.post(this.loginUrl, { user }, options)
  //     .map(this.extractData);
  // }
  private extractData(res: Response) {
    let body = res.json();
    // console.log(body);
    return body || { };
  }

  private handleError (error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
