/**
 * Created by thangdt on 17/01/2017.
 */
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { Post } from '../model/Post';
import { NewPostResponse } from '../model/NewPostResponse';
import { ListPostResponse } from '../model/ListPostResponse';
import { DeletePostResponse } from '../model/DeletePostResponse';

@Injectable()
export class PostService {
  public createPostUrl:string = 'posts/create';
  public getListPostUrl: string = 'posts/getlistpost';
  public deletePostUrl:string = 'posts/deletePost/';

  constructor(private http: Http){};

  createPost(newPost: Post): Observable<NewPostResponse> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    // console.log(newPost);
    return this.http.post(this.createPostUrl, { newPost }, options)
      .map(this.extractData);
  }

  getListPost(): Observable<ListPostResponse>{
    return this.http.get(this.getListPostUrl)
      .map(this.extractData);
  }

  deletePost(_id:string): Observable<DeletePostResponse> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.deletePostUrl,{_id},options).map(this.extractData);
  }

  private extractData(res: Response) {
    let body = res.json();
    // console.log(body);
    return body || { };
  }

  private handleError (error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
