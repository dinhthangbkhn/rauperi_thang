import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule, JsonpModule } from '@angular/http';
import { FormsModule }   from '@angular/forms';
import { requestOptionsProvider }   from './default-request-options.service';
import { CookieService} from 'angular2-cookie/services/cookies.service';


import { AppComponent }  from './app.component';
import { AboutComponent } from "./components/about/about.component";
import { routing } from "./routes";
import { HomeComponent } from "./components/home/home.component";
import { LoginComponent} from "./components/login/login.component";
import { MyproductComponent } from "./components/myproduct/myproduct.component";
import { UserService } from "./service/user.service";
import { MyproductService} from "./service/myproduct.service";
import { PostComponent } from "./components/post/post.component";
import { PostService} from "./service/post.service";
import { CommentService } from "./service/comment.service";
import { BillService } from "./service/bill.service";
import { StoreComponent } from "./components/store/store.component";
import { BoughtComponent } from "./components/bought/bought.component";
import { SubmitComponent } from "./components/submit/submit.component";

@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        FormsModule,
        JsonpModule,
        routing
    ],
    declarations: [
        AppComponent,
        AboutComponent,
        HomeComponent,
        LoginComponent,
        MyproductComponent,
        PostComponent,
        StoreComponent,
        BoughtComponent,
        SubmitComponent
    ],
    providers:[
      requestOptionsProvider,
      CookieService,
      UserService,
      MyproductService,
      PostService,
      CommentService,
      BillService
    ],
    bootstrap: [ AppComponent ]
})
export class AppModule { }
