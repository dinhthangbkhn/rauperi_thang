import { Route, RouterModule } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { AboutComponent } from './components/about/about.component';
import { LoginComponent } from './components/login/login.component';
import { MyproductComponent} from './components/myproduct/myproduct.component';
import { PostComponent } from './components/post/post.component';
import { StoreComponent} from './components/store/store.component';
import { BoughtComponent } from './components/bought/bought.component';
import { SubmitComponent } from './components/submit/submit.component';
export const routes: Route[] = [
    { path: '', pathMatch: 'full', component: HomeComponent },
    { path: 'about', component: AboutComponent },
    { path: 'login', component: LoginComponent},
    { path: 'myproduct', component: MyproductComponent},
    { path: 'post', component: PostComponent },
    { path: 'store', component: StoreComponent},
    { path: 'bought', component: BoughtComponent},
    { path: 'submit', component: SubmitComponent}
];

export const routing = RouterModule.forRoot(routes, { useHash: true });
