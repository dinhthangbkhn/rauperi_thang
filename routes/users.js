var express = require('express');
var router = express.Router();
var ObjectId = require('mongodb').ObjectID;
/* GET users listing. */
router.get('/:username', function(req, res, next) {
  // var data = req.body;

  var mongodb = require('mongodb');
  var server = new mongodb.Server('127.0.0.1', 27017, {});
  var client = new mongodb.Db('rauperi', server, {w: 1});
  client.open(function(err) {
    if (err) throw err;

    client.collection('User', function(err, collection) {

      if (err) throw err;
      console.log('We are now able to perform queries.');

      collection.find({"username":req.params.username}).toArray(
        function (err, results) {
          if(err) throw err;
          console.log(results);
          if(results.length != 0) {
            res.json(results[0]);
          }
        }
      )
    });
  });
});

router.get('/id/:userid',function (req, res, next) {
  var mongodb = require('mongodb');
  var server = new mongodb.Server('127.0.0.1', 27017, {});
  var client = new mongodb.Db('rauperi', server, {w: 1});
  console.log(typeof req.params.userid);
  var objId = new ObjectId(req.params.userid);
  console.log(objId);
  client.open(function(err) {
    if (err) throw err;

    client.collection('User', function(err, collection) {

      if (err) throw err;
      console.log('We are now able to perform queries.');

      collection.find({_id : objId}).toArray(
        function (err, results) {
          if(err) throw err;
          console.log(results);
          if(results.length != 0) {
            res.json(results[0]);
          }
        }
      )
    });
  });
});

router.post('/create',function (req, res, next) {
  var data = req.body.user;
  var mongodb = require('mongodb');
  var server = new mongodb.Server('127.0.0.1', 27017, {});
  var client = new mongodb.Db('rauperi', server, {w: 1});
  client.open(function(err) {
    if (err) throw err;

    client.collection('User', function(err, collection) {

      if (err) throw err;
      console.log('We are now able to perform queries.');

      collection.insert(data,
        {
          safe:true
        },
        function (err, documents) {
          if(err) throw err;
          // console.log('Document ID is '+documents[0]._id);
        });
      res.json({isSucceed:true});
    });
  });
});

module.exports = router;
