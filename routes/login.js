/**
 * Created by thangdt on 13/01/2017.
 */
var express = require('express');
var routerLogin = express.Router();

routerLogin.post('/',function (req, res, next) {
  // console.log(req.body);
  var data = req.body;
  console.log(data);
  var mongodb = require('mongodb');
  var server = new mongodb.Server('127.0.0.1', 27017, {});
  var client = new mongodb.Db('rauperi', server, {w: 1});

  client.open(function(err) {
    if (err) throw err;

    client.collection('User', function(err, collection) {

      if (err) throw err;
      console.log('We are now able to perform queries.');
      console.log(data);

      collection.find({"username":data.user.username}).toArray(
        function (err, results) {
          if(err) throw err;
          console.log(results);
          if(results.length==0) {
            res.json({ isCorrect: false, error: "username is wrong"});
          } else {
            if(results[0].password != data.user.password) {
              res.json({ isCorrect: false, error: "password is wrong"});
            } else {
              res.json({ isCorrect: true, error: ""});
            }
          }
        }
      )
    });
  });

});

routerLogin.get('/',function (req, res, next) {
  console.log("GET login");
});

module.exports = routerLogin;
