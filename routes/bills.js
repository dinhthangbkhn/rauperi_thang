/**
 * Created by thangdt on 19/01/2017.
 */

var express = require('express');
var routerBill = express.Router();
var ObjectId = require('mongodb').ObjectID;

routerBill.post('/create',function (req, res, next) {
  console.log(req.body);
  // console.log("xin chao anh em");
  var data = req.body.newBill;
  var mongodb = require('mongodb');
  var server = new mongodb.Server('127.0.0.1', 27017, {});
  var client = new mongodb.Db('rauperi', server, {w: 1});

  client.open(function(err) {
    if (err) throw err;
    var oldNumber = 0;
    var productid = new ObjectId(data.productid);
    client.collection('Product',function (err, collection) {
      if(err) throw err;
      collection.find({_id: productid}).toArray(
        function (err, results) {
          if(err) throw err;
          oldNumber = results[0].number;
        }
      );
    });

    client.collection('Product',function (err, collection) {
      if (err) throw err;
      collection.update(
        {_id: new ObjectId(data.productid)},
        { $set: {"number": oldNumber - data.number }},
        {safe: true},
        function (err) {
          if(err) throw err;
        }
      )
    });

    client.collection('Bill', function(err, collection) {

      if (err) throw err;

      collection.insert(data,
        {
          safe:true
        },
        function (err, documents) {
          if(err) throw err;
        });
      res.json({isSucceed:true});
    });


  });
});

routerBill.get('/getlistbillbyuserid/:userid',function (req, res, next) {
  console.log(req.params.userid);
  // var obj_id = new ObjectId('5875ef978d3d396f2951eb9a');
  // var obj_id = '5875ef978d3d396f2951eb9a';
  var mongodb = require('mongodb');
  var server = new mongodb.Server('127.0.0.1', 27017, {});
  var client = new mongodb.Db('rauperi', server, {w: 1});

  client.open(function (err) {
    if (err) throw err;

    client.collection('Bill', function (err, collection) {

      if (err) throw err;
      console.log('We are now able to perform queries.');

      collection.find({"userid": req.params.userid}).toArray(
        function (err, results) {
          if (err) throw err;
          console.log(results);
          if(results.length!=0) {
            res.json(
              {
                isSucceed:true,
                listBills: results
              }
            )
          } else {
            res.json(
              {
                isSucceed:false,
                listBills:[]
              }
            )
          }

        }
      )
    });
  });
});

module.exports = routerBill;
