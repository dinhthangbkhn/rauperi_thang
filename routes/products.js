/**
 * Created by thangdt on 16/01/2017.
 */
var express = require('express');
var routerProduct = express.Router();
var ObjectId = require('mongodb').ObjectID;

routerProduct.post('/create',function (req, res, next) {
  console.log(req.body);
  // console.log("xin chao anh em");
  var data = req.body.newProduct;
  var mongodb = require('mongodb');
  var server = new mongodb.Server('127.0.0.1', 27017, {});
  var client = new mongodb.Db('rauperi', server, {w: 1});

  client.open(function(err) {
    if (err) throw err;

    client.collection('Product', function(err, collection) {

      if (err) throw err;
      console.log('We are now able to perform product s queries.');
      console.log(data);

      collection.insert(data,
        {
          safe:true
        },
        function (err, documents) {
          if(err) throw err;
          // console.log('Document ID is '+documents[0]._id);
        });
      res.json({isSucceed:true});
      });
  });
});

routerProduct.get('/listProduct/:userid',function (req, res, next) {
  console.log(req.params.userid);
  // var obj_id = new ObjectId('5875ef978d3d396f2951eb9a');
  // var obj_id = '5875ef978d3d396f2951eb9a';
  var mongodb = require('mongodb');
  var server = new mongodb.Server('127.0.0.1', 27017, {});
  var client = new mongodb.Db('rauperi', server, {w: 1});

  client.open(function (err) {
    if (err) throw err;

    client.collection('Product', function (err, collection) {

      if (err) throw err;

      collection.find({"userid": req.params.userid}).toArray(
        function (err, results) {
          if (err) throw err;
          console.log(results);
          if(results.length!=0) {
            res.json(
              {
                isSucceed:true,
                listProduct: results
              }
            )
          } else {
            res.json(
              {
                isSucceed:false,
                listProduct:[]
              }
            )
          }

        }
      )
    });
  });
});

routerProduct.get('/getAllProducts',function (req, res, next) {
  var mongodb = require('mongodb');
  var server = new mongodb.Server('127.0.0.1', 27017, {});
  var client = new mongodb.Db('rauperi', server, {w: 1});
  client.open(function (err) {
    if(err) throw err;
    client.collection('Product',function (err, collection) {
      if(err) throw err;
      collection.find().toArray(
        function (err, results) {
          if(err) throw err;
          console.log(results);
          res.json({
              isSucceed:true,
              listProduct:results
          });
        }
      )
    })
  })
});

routerProduct.get('/id/:productid',function (req, res, next) {
  var mongodb = require('mongodb');
  var server = new mongodb.Server('127.0.0.1', 27017, {});
  var client = new mongodb.Db('rauperi', server, {w: 1});
  var objId = new ObjectId(req.params.productid);
  client.open(function (err) {
    if(err) throw err;
    client.collection('Product', function (err, collection) {
      if (err) throw err;
      collection.find({_id:objId}).toArray(
        function (err, results) {
          if (err) throw err;
          res.json(results[0]);

        }
      )
    });
  });

})
module.exports = routerProduct;
