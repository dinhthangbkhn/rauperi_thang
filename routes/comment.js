/**
 * Created by thangdt on 18/01/2017.
 */

var express = require('express');
var routerComment = express.Router();
var ObjectId = require('mongodb').ObjectID;

routerComment.post('/create',function (req, res, next) {
  console.log(req.body);
  var data = req.body.newComment;
  var mongodb = require('mongodb');
  var server = new mongodb.Server('127.0.0.1', 27017, {});
  var client = new mongodb.Db('rauperi', server, {w: 1});

  client.open(function(err) {
    if (err) throw err;

    client.collection('Comment', function(err, collection) {

      if (err) throw err;
      console.log('We are now able to perform product s queries.');
      console.log(data);

      collection.insert(data,
        {
          safe:true
        },
        function (err, documents) {
          if(err) throw err;
          // console.log('Document ID is '+documents[0]._id);
        });
      res.json({isSucceed:true});
    });
  });
});



routerComment.get('/getListComment/:postid', function (req, res, next) {
  var mongodb = require('mongodb');
  var server = new mongodb.Server('127.0.0.1', 27017, {});
  var client = new mongodb.Db('rauperi', server, {w: 1});
  client.open(function (err) {
    if (err) throw err;

    client.collection('Comment', function (err, collection) {

      if (err) throw err;
      console.log('We are now able to perform queries.');

      collection.find({"postid":req.params.postid}).toArray(
        function (err, results) {
          if (err) throw err;
          console.log(results);
          if(results.length!=0) {
            res.json(
              {
                isSucceed:true,
                listComment: results
              }
            )
          } else {
            res.json(
              {
                isSucceed:false,
                listComment:[]
              }
            )
          }

        }
      )
    });
  });
});

module.exports = routerComment;
