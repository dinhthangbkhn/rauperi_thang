/**
 * Created by thangdt on 17/01/2017.
 */
var express = require('express');
var routerPost = express.Router();
var ObjectId = require('mongodb').ObjectID;

routerPost.post('/create',function (req, res, next) {
  console.log(req.body);
  var data = req.body.newPost;
  var mongodb = require('mongodb');
  var server = new mongodb.Server('127.0.0.1', 27017, {});
  var client = new mongodb.Db('rauperi', server, {w: 1});

  client.open(function(err) {
    if (err) throw err;

    client.collection('Post', function(err, collection) {

      if (err) throw err;
      console.log('We are now able to perform product s queries.');
      console.log(data);

      collection.insert(data,
        {
          safe:true
        },
        function (err, documents) {
          if(err) throw err;
          // console.log('Document ID is '+documents[0]._id);
        });
      res.json({isSucceed:true});
    });
  });
});



routerPost.get('/getListPost', function (req, res, next) {
  var mongodb = require('mongodb');
  var server = new mongodb.Server('127.0.0.1', 27017, {});
  var client = new mongodb.Db('rauperi', server, {w: 1});
  client.open(function (err) {
    if (err) throw err;

    client.collection('Post', function (err, collection) {

      if (err) throw err;
      console.log('We are now able to perform queries.');

      collection.find().toArray(
        function (err, results) {
          if (err) throw err;
          console.log(results);
          if(results.length!=0) {
            res.json(
              {
                isSucceed:true,
                listPost: results
              }
            )
          } else {
            res.json(
              {
                isSucceed:false,
                listPost:[]
              }
            )
          }

        }
      )
    });
  });
});

routerPost.post('/deletePost',function (req, res, next) {
  var _id = req.body._id;
  console.log("delete post");
  console.log(req.body);
  var obj_id = new ObjectId(_id);
  // console.log(typeof obj_id);
  var mongodb = require('mongodb');
  var server = new mongodb.Server('127.0.0.1', 27017, {});
  var client = new mongodb.Db('rauperi', server, {w: 1});
  client.open(function (err) {
    if (err) throw err;

    client.collection('Post', function (err, collection) {

      if (err) throw err;
      console.log('We are now able to perform queries.');

      collection.remove({_id : obj_id},{safe:true},function (err) {
        if(err) throw err
      });

      res.json(
        {
          isSucceed: true
        }
      );

    });
  });
});
module.exports = routerPost;
